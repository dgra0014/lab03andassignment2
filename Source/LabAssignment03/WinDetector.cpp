// Fill out your copyright notice in the Description page of Project Settings.


#include "WinDetector.h"

// Sets default values
AWinDetector::AWinDetector()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
}

// Called when the game starts or when spawned
void AWinDetector::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWinDetector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Target != nullptr)
	{
		FVector loc = Target->GetActorLocation();
		if(loc.X > 400 && loc.Y > 800)
			FGenericPlatformMisc::RequestExit(false);
	}
}

