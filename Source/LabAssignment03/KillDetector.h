// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "KillDetector.generated.h"

UCLASS()
class LABASSIGNMENT03_API AKillDetector : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AKillDetector();

	UPROPERTY(EditAnywhere)
		AActor* Target;
	UPROPERTY(EditAnywhere)
		float Height = 0.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
