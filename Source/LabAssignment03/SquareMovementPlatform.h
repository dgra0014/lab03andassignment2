// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UObject/ConstructorHelpers.h"
#include "SquareMovementPlatform.generated.h"

UCLASS()
class LABASSIGNMENT03_API ASquareMovementPlatform : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* VisibleComponent;
	
public:	
	// Sets default values for this actor's properties
	ASquareMovementPlatform();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FVector Start, End;
	int currEndCode;
	float Speed, Tolerance;
	FVector getNextEnd();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
