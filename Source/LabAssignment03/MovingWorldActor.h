// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UObject/ConstructorHelpers.h"
#include "MovingWorldActor.generated.h"

UCLASS()
class LABASSIGNMENT03_API AMovingWorldActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMovingWorldActor();

	UPROPERTY(EditAnywhere)
		float TravelDistance;
	UPROPERTY(EditAnywhere)
		float Speed;
	UPROPERTY(EditAnywhere)
		float Tolerance;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* VisibleComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FVector StartingPosition;
	FVector TargetPosition;
	float Direction;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
