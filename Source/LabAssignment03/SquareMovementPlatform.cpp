// Fill out your copyright notice in the Description page of Project Settings.


#include "SquareMovementPlatform.h"

// Sets default values
ASquareMovementPlatform::ASquareMovementPlatform()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(RootComponent);

	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshObj(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));
	if (CubeMeshObj.Succeeded())
	{
		VisibleComponent->SetStaticMesh(CubeMeshObj.Object);
	}

	Start = FVector(0, 0, 0);
	End = FVector(0, 0, 0);
	Speed = 200;
	Tolerance = 10;
	currEndCode = 0;
}

// Called when the game starts or when spawned
void ASquareMovementPlatform::BeginPlay()
{
	Super::BeginPlay();
	
	Start = GetActorLocation();
	End = getNextEnd();
}

FVector ASquareMovementPlatform::getNextEnd()
{
	int c = currEndCode;
	currEndCode++;
	if (currEndCode > 3) currEndCode = 0;

	if (c == 0)
		return FVector(800, -1000, 700);
	else if (c == 1)
		return FVector(800, 800, 700);
	else if (c == 2)
		return FVector(-1200, 800, 700);
	else// if (c == 3)
		return FVector(-1200, -1000, 700);
}

// Called every frame
void ASquareMovementPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector direction = End - Start;
	direction.Normalize();
	FVector currPos = GetActorLocation();

	currPos += direction * Speed * DeltaTime;

	if (FVector::Dist(currPos, End) <= Tolerance)
	{
		currPos = End;
		End = getNextEnd();
		Start = currPos;
	}

	SetActorLocation(currPos);
}

