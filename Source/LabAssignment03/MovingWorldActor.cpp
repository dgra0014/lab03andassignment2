// Fill out your copyright notice in the Description page of Project Settings.


#include "MovingWorldActor.h"

// Sets default values
AMovingWorldActor::AMovingWorldActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(RootComponent);

	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshObj(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));
	if (CubeMeshObj.Succeeded())
	{
		VisibleComponent->SetStaticMesh(CubeMeshObj.Object);
	}

	TravelDistance = 200;
	Speed = 200;
	Tolerance = 10;
	Direction = 1;
}

// Called when the game starts or when spawned
void AMovingWorldActor::BeginPlay()
{
	Super::BeginPlay();
	
	const FVector travelDir = FVector(1, 0, 0) * TravelDistance;
	StartingPosition = GetActorLocation();
	TargetPosition = StartingPosition + travelDir * Direction;
}

// Called every frame
void AMovingWorldActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector direction = FVector(1, 0, 0) * Direction;
	FVector currPos = GetActorLocation();

	currPos += direction * Speed * DeltaTime;

	if (FVector::Dist(currPos, TargetPosition) <= Tolerance)
	{
		currPos = TargetPosition;
		TargetPosition = StartingPosition;
		StartingPosition = currPos;
		Direction *= -1;
	}

	SetActorLocation(currPos);
}

