// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LabAssignment03GameMode.generated.h"

UCLASS(minimalapi)
class ALabAssignment03GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALabAssignment03GameMode();
};



