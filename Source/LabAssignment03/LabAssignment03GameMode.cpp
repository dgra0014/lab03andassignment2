// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "LabAssignment03GameMode.h"
#include "LabAssignment03Character.h"
#include "UObject/ConstructorHelpers.h"

ALabAssignment03GameMode::ALabAssignment03GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
