// Fill out your copyright notice in the Description page of Project Settings.


#include "KillDetector.h"

// Sets default values
AKillDetector::AKillDetector()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

}

// Called when the game starts or when spawned
void AKillDetector::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AKillDetector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (Target != nullptr)
	{
		FVector loc = Target->GetActorLocation();
		if(loc.Z < Height)
			FGenericPlatformMisc::RequestExit(false);
	}
}

